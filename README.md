Thanks for considering a contribution to GitLab's
[built-in project templates](https://docs.gitlab.com/ee/development/project_templates.html).

Full instructions for contributing can be found in our [Contributing to Project Templates instructions](https://about.gitlab.com/community/contribute/project-templates) page.
